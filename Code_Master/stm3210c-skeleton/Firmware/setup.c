#include "setup.h"
#include "global.h"
#include <stm32f10x.h>
#include <system_stm32f10x_cl.h>
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_i2c.h"
#include "stm3210c_eval_ioe.h"

/* Private variables ---------------------------------------------------------*/
I2C_InitTypeDef   I2C_InitStructure;
NVIC_InitTypeDef  NVIC_InitStructure;
GPIO_InitTypeDef  GPIO_InitStructure;

/*
 * Configure the clocks, GPIO and other peripherals.
 */
void prvSetupHardware( void )
{
  /* Set the clocks. */
  SystemInit();
  RCC->APB2ENR |= 0x00000261;

  /* Configure the GPIO for LEDs. */
  GPIOD->CRL &= 0xFFF00FFF;
  GPIOD->CRL |= 0x00033000;
  GPIOD->CRH &= 0xFF0FFFFF;
  GPIOD->CRH |= 0x00300000;
  GPIOE->CRH &= 0xF0FFFFFF;
  GPIOE->CRH |= 0x03000000;

  /* Configure UART2 for 115200 baud. */
  AFIO->MAPR |= 0x00000008;
  GPIOD->CRL &= 0xF00FFFFF;
  GPIOD->CRL |= 0x04B00000;

  RCC->APB1ENR |= 0x00020000;
  USART2->BRR = 0x0135;
  USART2->CR3 = 0x0000;
  USART2->CR2 = 0x0000;
  USART2->CR1 = 0x200C;

  /* Configure ADC.14 input. */
  GPIOC->CRL &= 0xFFF0FFFF;
  ADC1->SQR1  = 0x00000000;
  ADC1->SMPR1 = (5<<12);
  ADC1->SQR3  = (14<<0);
  ADC1->CR1   = 0x00000100;
  ADC1->CR2   = 0x000E0003;

  /* Reset calibration */
  ADC1->CR2  |= 0x00000008;
  while (ADC1->CR2 & 0x00000008);

  /* Start calibration */
  ADC1->CR2  |= 0x00000004;
  while (ADC1->CR2 & 0x00000004);
  ADC1->CR2  |= 0x00500000;

  /* Setup and enable the SysTick timer for 100ms. */
  SysTick->LOAD = (SystemFrequency / 100) - 1;
  SysTick->CTRL = 0x05;
}
/*-----------------------------------------------------------*/

/* Function to execute if an assertion failed */
void assert_failed(u8* file, u32 line) {
  errorMessage("ERROR UNKNOWN:      assert_failed");
}






void I2C_Configuration(void){
	/* Enable I2C1 and I2C2 ----------------------------------------------------*/
	I2C_Cmd(I2C1, ENABLE);

	/* I2C1 configuration ------------------------------------------------------*/
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = I2C1_MY_ADDRESS;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = ClockSpeed;
	I2C_Init(I2C1, &I2C_InitStructure);
	
	/* Configure I2C1 pins: SCL and SDA ----------------------------------------*/
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;  // Open Drain, I2C bus pulled high externally
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	// Enable the I2C1 module:
  I2C_Cmd(I2C1, ENABLE);
}

void NVIC_Configuration(void)
{
  /* Configure and enable I2C1 event interrupt -------------------------------*/
  NVIC_InitStructure.NVIC_IRQChannel = I2C1_EV_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**************************************************************************************/

void RCC_Configuration(void)
{
  /* Enable peripheral clocks ------------------------------------------------*/
  /* Enable GPIOB clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	/* Enable GPIOA clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	/* Enable AFIO clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	/* Enable I2C1 clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
}

/**************************************************************************************/

void GPIO_Configuration(void)
{
  GPIO_InitTypeDef LEDInit;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure LED pins: -----------------------------------------------------*/
	GPIO_StructInit(&LEDInit);
	// Pin 7 = green     Pin 3 = red
	LEDInit.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_13;
	LEDInit.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOD, &LEDInit);
	GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_RESET);
	GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);
	
	GPIO_StructInit(&GPIO_InitStructure);
	//using same structure we will initialize button pin
  //select pin to initialize button
  GPIO_InitStructure.GPIO_Pin = 3;
  //select input floating
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}
