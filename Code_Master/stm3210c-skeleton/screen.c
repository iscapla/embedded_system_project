#include "FreeRTOS.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm3210c_eval_lcd.h"
#include "stm3210c_eval_ioe.h"
#include <string.h>
#include "task.h"
#include "screen.h"
#include "global.h"
#include "assert.h"

void showGrid(void);
void showPoint(int x, int y);

xTaskHandle screenHandle;

// Present the grid on the screen
void showGrid(){
	int i;
	LCD_Clear(White);
	LCD_SetBackColor(White);
	LCD_SetTextColor(Black);
	// Draw the Y axis
	LCD_DrawLine(MAX_HEIGHT/2, MAX_WIDTH, MAX_WIDTH, LCD_DIR_HORIZONTAL);
	for(i=20; i<=MAX_WIDTH-INTERVAL; i+=INTERVAL){
		if(i != MAX_WIDTH/2) {
			LCD_SetTextColor(Cyan);
			LCD_DrawLine(0, (MAX_WIDTH-i), MAX_HEIGHT, LCD_DIR_VERTICAL);
			LCD_SetTextColor(Black);
			LCD_DrawLine((MAX_HEIGHT/2 - 2), (MAX_WIDTH - i), 5, LCD_DIR_VERTICAL);
		}
	}
	// Draw the X axis
	LCD_DrawLine(0, MAX_WIDTH/2, MAX_HEIGHT, LCD_DIR_VERTICAL);
	for(i=20; i<=MAX_HEIGHT-INTERVAL; i+=INTERVAL){
		if(i != MAX_HEIGHT/2){
			LCD_SetTextColor(Cyan);
			LCD_DrawLine(i, MAX_WIDTH, MAX_WIDTH, LCD_DIR_HORIZONTAL);
			LCD_SetTextColor(Black);
			LCD_DrawLine(i, (MAX_WIDTH/2 + 2), 5, LCD_DIR_HORIZONTAL);
		}
	}
	
	// Draw microphones
	for(i=0;i<MICROPHONES;i++){
		LCD_SetBackColor(Green);
		LCD_DrawFullCircle(MAX_HEIGHT - micPos[i].x, MAX_WIDTH - micPos[i].y-40, 4);
	}
}

// Present the point on the grid
void showPoint(int x, int y){
	showGrid();
	LCD_SetBackColor(Red);
	
	// Put the point in the border of the screen in case of overflow it.
	x = MIN(MAX_HEIGHT,x);
	y = MIN(MAX_HEIGHT,y);
	
	LCD_DrawFullCircle(MAX_HEIGHT-x, MAX_WIDTH-y-40, 4);
}

// Handle error messages
void errorMessage(char *p){
	int i,j;
	int length;
	int const max = 20;
	uint8_t text[3][max];
	length = strlen(p);
	
	length = (length > max*3) ? max*3 : length;
	
	// Clean all the buffers
	for (i=0;i<3;i++)
		for(j=0;j<max;j++)
			text[i][j]=(uint8_t)' ';

	for(i=0;i<length;i++){
		text[i/max][i%max] = p[i];
	}
	
	// To avoid possible problems with the screen task, it is suspended.
	vTaskSuspend(screenHandle);
	
	while(1){
		LCD_Clear(Black);
		LCD_SetTextColor(Red);
		LCD_SetBackColor(Black);
		LCD_DisplayStringLine(Line2," Error:");
		LCD_DisplayStringLine(Line3,text[0]);
		LCD_DisplayStringLine(Line4,text[1]);
		LCD_DisplayStringLine(Line5,text[2]);
		LCD_DisplayStringLine(Line7,"     - RESTART -    ");
		vTaskDelay(3000/portTICK_RATE_MS);
	}
}

// Function to show a integer in the screen as a debugger
void showNumLine(int num, uint8_t line){
	int i;
	int const max = 20;
	int aux = num;
	uint8_t text[max];
	
	for (i=0;i<max;i++){text[i]=(uint8_t)'0';}
	
	for (i=max-1;i>0;i--){
		text[i] = (aux%10)+'0';
		aux=aux/10;
	}
	
	// To avoid possible problems with the screen task, it is suspended.
	vTaskSuspend(screenHandle);
	
	//LCD_Clear(Black);
	LCD_SetTextColor(Red);
	LCD_SetBackColor(Black);
	LCD_DisplayStringLine(line,text);
	vTaskDelay(100/portTICK_RATE_MS);
}


void ScreenTask(void *params){
	Point3D centroid;
	while(1){
		while(xSemaphoreTake(xSemaphoreCentroid, portMAX_DELAY) != pdTRUE);
		centroid = g_Centroid;
		xSemaphoreGive(xSemaphoreCentroid);

		showPoint(centroid.x,centroid.y);
		vTaskDelay(50/portTICK_RATE_MS);
	}
}

void setupScreen(void){
	portBASE_TYPE res;
	
	res = xTaskCreate(ScreenTask, "screen", 100, NULL, 1, &screenHandle);
	
	assert(res == pdTRUE);
}
