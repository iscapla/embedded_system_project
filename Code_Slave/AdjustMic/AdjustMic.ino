#define NUM_MICROPHONES 4   // Number of microphones used in this project
#define SAMPLE_WINDOW   100  // Sample window width in mS (100 mS ~ 10Hz)

// Analog input pins for the microphones
const int micPos[NUM_MICROPHONES] = {A0, A1, A2, A3};

unsigned int peakToPeak[NUM_MICROPHONES];
unsigned int signalMax[NUM_MICROPHONES];
unsigned int signalMin[NUM_MICROPHONES];

// Auxiliar variables
unsigned int sample;
unsigned long startMillis;
int i;

void setup() {
  Serial.begin(9600);             // Start serial for output
}


void loop() {
  // Reseting the values
  for (i = 0; i < NUM_MICROPHONES; i++) {
    peakToPeak[i]  = 0;   // peak-to-peak level
    signalMax[i]   = 0;
    signalMin[i]   = 1024;
  }

  // Getting the current time of the arduino
  startMillis = millis();  // Start of sample window

  // Collect data for 66 mS
  while (millis() - startMillis < SAMPLE_WINDOW) {
    sample = 0;
    for (i = 0; i < NUM_MICROPHONES; i++) {
      sample = analogRead(micPos[i]); // Read the analog value. It returns 2 bytes (unsigned int in the Yun board)

      if (sample < 1024) { // toss out spurious readings
        if (sample > signalMax[i]) {
          signalMax[i] = sample;  // save just the max levels
        }
        else if (sample < signalMin[i]) {
          signalMin[i] = sample;  // save just the min levels
        }
      }
    }
  }


  // Calculating the final result of the reading
  for (i = 0; i < NUM_MICROPHONES; i++) {
    peakToPeak[i] = signalMax[i] - signalMin[i];  // max - min = peak-peak amplitude
  }

  Serial.print(peakToPeak[0]);
  Serial.print("\t");
  Serial.print(peakToPeak[1]);
  Serial.print("\t");
  Serial.print(peakToPeak[2]);
  Serial.print("\t");
  Serial.print(peakToPeak[3]);
  Serial.println("");


  //delay(100);
}
