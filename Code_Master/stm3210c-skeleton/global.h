/**
 * This file defines datastructures used for communication between
 * the various modules
 */
#ifndef GLOBAL_H
#define GLOBAL_H

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#include "FreeRTOS.h"
#include "stm32f10x.h"
#include "assert.h"
#include "queue.h"
#include "semphr.h"
#include "stm32f10x.h"
#include "screen.h"

typedef enum {MEMORY = 0, MICROPHONE = !MEMORY} myMode;

#define MICROPHONES 4

// Grid dimensions
#define MAX_HEIGHT 240 // X axis
#define MAX_WIDTH  320  // Y axis
#define INTERVAL    20    // Axis interval's length
// I2C variables
#define I2C1_MY_ADDRESS		0x15
#define ClockSpeed      100000
#define SLAVE_DIR 				0x20
#define CHANGE_MODE_COD 		80
#define RESET_COD 				 100

// Structure to represent and share 3d points between the taskes.
typedef struct {
	float x,y,z;
}Point3D;

// Semaphore to interconnect Microphones Task and Algorithm Task
extern xSemaphoreHandle xSemaphore;
extern xSemaphoreHandle xSemaphoreCentroid;
extern xSemaphoreHandle xSemaphoreStatus;

// Array to allocate the microphones reads
extern Point3D micPos[MICROPHONES];
extern float micReads[MICROPHONES];
extern Point3D g_Centroid;
extern int mask;
extern myMode status;

// Externalize the functions
void initializeSemaphore(void);
void initializeQueue(void);
void initializeData(void);
#endif
