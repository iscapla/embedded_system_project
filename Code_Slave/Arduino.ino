/*
 * ***************************************************************************************
 * ******                       Embedded System Project - Slave                     ******
 * ***************************************************************************************
 *  Uppsala University - May 2016
 *
 *  Components:
 *    Casabán Planells, Ismael  ->  ismaelcasaban@live.com
 *    Kweun, Min Hyeok          ->  minhyeok.Kweun.4523@student.uu.se
 *    Stoyanova, Desislava      ->  desislava.Stoyanova.4381@student.uu.se
 *
 *  Resume:
 *    This module implements a communication via I2C with a Arduino Yún and a STM3210c-Eval (using STM32f10x).
 *    The objective is to read 4 analog values from diferent microphones all the time and when a request is received,
 *    the data is sended.
 *
 *  Pins used:
 *    A0, A1, A2, A3 -> Microphones
 *    D2 (SDA in Arduino Yun)
 *    D3 (SCL in Arduino Yun)
 *
 */

#include <Wire.h> // Contains all the operations needed to connect the arduino with the stm board

#define DEBUG 0
#define NUM_POINTS 26

#define NUM_MICROPHONES 4   // Number of microphones used in this project
#define SAMPLE_WINDOW   50  // Sample window width in mS (100 mS ~ 10Hz)

#define MY_I2C_DIR 0x20     // My own address in the I2C bus
#define CLOCK_SPEED 100000L  // Clock speed to use in the bus. The same as the stm32 board use.

#define RESET_CODE 100
#define CHANGE_MODE_CODE 80

typedef enum {MEMORY = 0, MICROPHONE = !MEMORY} myMode;
myMode mode = MICROPHONE;

unsigned int data[NUM_POINTS][NUM_MICROPHONES]{
  {71,196,269,196},
  {78,187,262,199},
  {86,177,255,202},
  {94,168,248,206},
  {103,158,242,210},
  {112,149,236,215},
  {121,139,230,220},
  {130,130,225,225},
  {139,121,220,230},
  {149,112,215,236},
  {158,103,210,242},
  {168,94,206,248},
  {177,86,202,255,},
  {187,78,199,262},
  {196,71,196,269},
  {206,64,194,276},
  {209,72,184,269},
  {212,81,175,262},
  {215,89,165,256},
  {219,98,155,250},
  {224,108,146,244},
  {228,117,136,239},
  {233,126,126,233},
  {239,136,117,228},
  {244,146,108,224},
  {250,155,98,219}
};  
int pos;

// Analog input pins for the microphones
const int micPos[NUM_MICROPHONES] = {A0, A1, A2, A3};

unsigned int peakToPeak[NUM_MICROPHONES];
unsigned int signalMax[NUM_MICROPHONES];
unsigned int signalMin[NUM_MICROPHONES];
unsigned char toSend[NUM_MICROPHONES *2];

// General functions
void software_Reset();          // Restarts program from beginning but does not reset the peripherals and registers
void receiveEvent(int howMany); // To receive data. Function registered.
void requestEvent();            // To send data. Function registered.
void intToCharArray(unsigned int *from, char *to, unsigned int num);  // To convert the array of integers to array of chars

// Auxiliar variables
unsigned int sample;
unsigned long startMillis;
int i;

void setup() {
  Wire.begin(MY_I2C_DIR);         // Join i2c bus with my address
  Wire.setClock(CLOCK_SPEED);     // Set the clock speed to 100KHz
  Wire.onReceive(receiveEvent);   // Register event - On event trying to write on me
  Wire.onRequest(requestEvent);   // Register event - On event trying to read from me
  
  Serial.begin(9600);             // Start serial for output
}


void loop() {
  // Reseting the values
  for (i = 0; i < NUM_MICROPHONES; i++) {
    //peakToPeak[i]  = 0;   // peak-to-peak level
    signalMax[i]   = 0;
    signalMin[i]   = 1024;
  }
  
  // Getting the current time of the arduino
  startMillis = millis();  // Start of sample window

  // Collect data for 100 mS
  while (millis() - startMillis < SAMPLE_WINDOW) {
    sample = 0;
    for (i = 0; i < NUM_MICROPHONES; i++) {
      sample = analogRead(micPos[i]); // Read the analog value. It returns 2 bytes (unsigned int in the Yun board)

      if (sample > signalMax[i]) {
        signalMax[i] = sample;  // save just the max levels
      }
      else if (sample < signalMin[i]) {
        signalMin[i] = sample;  // save just the min levels
      }
    }
  }


  // Calculating the final result of the reading
  for (i = 0; i < NUM_MICROPHONES; i++) {
    peakToPeak[i] = signalMax[i] - signalMin[i];  // max - min = peak-peak amplitude
  }
}

// Restarts program from beginning but does not reset the peripherals and registers
void software_Reset() {
  asm volatile ("  jmp 0");
}

// Function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  if (DEBUG)Serial.println(howMany);

  // Here we can read more than 1 byte but we couldn't send more than 1 byte with the stm32 board.
  while (1 < Wire.available()) { // loop through all but the last
    char c = Wire.read(); // receive byte as a character
    Serial.println(c);         // print the character
  }
  int x = Wire.read();    // receive byte as an integer
  if (DEBUG)Serial.println(x);        // print the integer


  switch(x){
      // Restart the program
      case RESET_CODE:{
        software_Reset();
        break;
      }
      case CHANGE_MODE_CODE:{
        if(mode == MICROPHONE){
          mode = MEMORY;  
        }else{
          mode = MICROPHONE;  
        }
        break;
      }
  }


}

// Function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  int i;
  if(mode){
    intToCharArray(peakToPeak, toSend, NUM_MICROPHONES);
  }else{
    intToCharArray(data[pos], toSend, NUM_MICROPHONES);
    pos++;
    if (pos >= NUM_POINTS){
      pos = 0;
    }
  }
 
  // Respond with message as expected by master.
  // In total we have to send 4 int => 8 Bytes
  Wire.write(toSend, 8);
}

void intToCharArray(unsigned int *from, unsigned char *to, unsigned int num) {
  int i;
  
  for (i = 0; i < num; i++) {
    to[(i * 2)]     = 0;
    to[(i * 2)]     = ((char)from[i]) & 0xFF;
    to[(i * 2) + 1] = 0;
    to[(i * 2) + 1] = ((char)(from[i] >> 8)) & 0xFF;
  }  
}
