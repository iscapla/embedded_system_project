#include <Wire.h>
int x = 15;
char y[4] = {'1','2','3','4'};
void setup() {
  Wire.begin(0x20);                // join i2c bus with address 0x10
  Wire.setClock(50000L);           // Set the clock speed to 100KHz
  Wire.onRequest(requestEvent); // register event
  Serial.begin(9600);           // start serial for output
}

void loop() {
  delay(100);
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Serial.println("R");
  Wire.write(y); // respond with message as expected by master
  y[0]++;
  y[1]+=2;
  y[2]+=3;
  y[3]+=5;
}
