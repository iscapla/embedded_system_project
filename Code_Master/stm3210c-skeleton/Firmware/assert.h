// Debugging: enable "assert"
#include "Firmware\stm32f10x_conf.h"
#undef assert
#define assert(expr) ((expr) ? (void)0 : assert_failed(__FILE__, __LINE__))
#define assert_param2(expr) ((expr) ? (void)0 : assert_failed(__FILE__, __LINE__))
void assert_failed(u8* file, u32 line);

