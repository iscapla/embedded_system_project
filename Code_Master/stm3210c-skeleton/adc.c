//Program NOT used in this project. But it works :)

#include "adc.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_lib.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_adc.h"
#include "Firmware/inc/stm32f10x_adc.h"
#include "stm32f10x_dma.h"

#define ADC1_DR_Address    ((uint32_t)0x4001244C)
#define ADC2_DR_Address    ((uint32_t)0x4001284C)

__IO uint32_t ADC1_DualConvertedValueTable[2] = {0, };
__IO uint32_t ADC2_DualConvertedValueTable[2] = {0, };
 
void setupADCReader(){
	ADC_InitTypeDef ADCInit;
	DMA_InitTypeDef DMA_InitStructure;
	GPIO_InitTypeDef MICInit;

	// configuration for input
	RCC_AHBPeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_StructInit(&MICInit);
	MICInit.GPIO_Pin = GPIO_Pin_0;
	MICInit.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOB, &MICInit);

	RCC_AHBPeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_StructInit(&MICInit);
	MICInit.GPIO_Pin =  GPIO_Pin_7;
	MICInit.GPIO_Mode = GPIO_Mode_AIN ;
	GPIO_Init(GPIOA, &MICInit);

	RCC_ADCCLKConfig(RCC_PCLK2_Div8);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
     
	// Clock configuration for ADC1
	RCC_ADCCLKConfig(RCC_PCLK2_Div6); //clock for ADC (max 14MHz, 72/6=12MHz)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); //enable ADC clock
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE); //enable ADC clock

     
  // DMA1 channel1 configuration ----------------------------------------------
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC1_DR_Address;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ADC1_DualConvertedValueTable;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = 2;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word; // 32bit
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word; // 32bit
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
	
  // Enable DMA1 Channel1
  DMA_Cmd(DMA1_Channel1, ENABLE);
  
	ADC_DeInit(ADC1);
	ADC_StructInit(&ADCInit);
	ADCInit.ADC_Mode = ADC_Mode_Independent;
	ADCInit.ADC_ScanConvMode = ENABLE;
	ADCInit.ADC_ContinuousConvMode = ENABLE;   // diff
	ADCInit.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; 
	ADCInit.ADC_DataAlign = ADC_DataAlign_Right;
	ADCInit.ADC_NbrOfChannel  = 2;     
     
	ADC_Init(ADC1, &ADCInit);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 1, ADC_SampleTime_55Cycles5); //PB0 as Input
	ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 2, ADC_SampleTime_55Cycles5); //PA7 as Input
	//  ADC_RegularChannelConfig(ADC2, ADC_Channel_10, 3, ADC_SampleTime_55Cycles5); //PC0 as Input
	//  ADC_RegularChannelConfig(ADC2, ADC_Channel_11, 4, ADC_SampleTime_55Cycles5); //PC1 as Input
	ADC_DMACmd(ADC1, ENABLE);
     
	//enable ADC to work
	ADC_Cmd(ADC1, ENABLE);     
	//Calibrate ADC *optional?
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
};
 
 //get Analog Value at pin
uint16_t getDigitalValue(ADC_TypeDef *ADCx, uint8_t Channel){
	ADC_RegularChannelConfig(ADCx, Channel, 1, ADC_SampleTime_1Cycles5); //PA6 as Input
	ADC_SoftwareStartConvCmd(ADCx, ENABLE);
	while(ADC_GetFlagStatus(ADCx, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADCx);
}
