#include "button.h"
#include "global.h"
#include "task.h"
#include "stm32f10x.h"
#include "stm32_eval.h"

#include "i2c.h"

void buttonTask (void *param){
	STM_EVAL_PBInit(Button_KEY,Mode_GPIO);
	while(1){
		/* Test if Key Button GPIO Pin level is low (Key push-button on Eval Board pressed) */
		if (STM_EVAL_PBGetState(Button_KEY) == 0x00){
			//		/*----- Transmission Phase -------------------------------------------------*/	
			I2C_StartTransmission(I2C1,I2C_Direction_Transmitter,SLAVE_DIR);
			I2C_WriteData(I2C1,CHANGE_MODE_COD);
			I2C_GenerateSTOP(I2C1, ENABLE);
			
			while(xSemaphoreTake(xSemaphoreStatus, portMAX_DELAY) != pdTRUE);
			if (status == MICROPHONE){
				status = MEMORY;
			}else{
				status = MICROPHONE;
			}
			xSemaphoreGive(xSemaphoreStatus);
			
			blinkLed(PASSED);
		}
		vTaskDelay(100/portTICK_RATE_MS);
	}
}

void setupButton(void){
portBASE_TYPE res;

  res = xTaskCreate(buttonTask, "button",100, NULL, 1, NULL);
									
  assert(res == pdTRUE);
}
