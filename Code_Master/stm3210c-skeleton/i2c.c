#include "i2c.h"
#include "global.h"
#include "task.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

/**************************************************************************************/

void blinkLed(TestStatus result){
	if(result){
		GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_SET);
		vTaskDelay(400/portTICK_RATE_MS);
		GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);
	}else{
		GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_SET);
		vTaskDelay(400/portTICK_RATE_MS);
		GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_RESET);
	}
	vTaskDelay(300/portTICK_RATE_MS);
}


void I2C_StartTransmission(I2C_TypeDef* I2Cx, uint8_t transmissionDirection,  uint8_t slaveAddress){
  // Wait until I2C module is idle:
  while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
  
  // Generate the start condition
  I2C_GenerateSTART(I2Cx, ENABLE);
  // 
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));
  
  //Send the address of the slave to be contacted:
  I2C_Send7bitAddress(I2Cx, slaveAddress<<1, transmissionDirection);
  
  //If this is a write operation, set I2C for transmit
  if(transmissionDirection== I2C_Direction_Transmitter)
    {
      while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    }
  //Or if a read operation, set i2C for receive
  if(transmissionDirection== I2C_Direction_Receiver)
    {
      while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
    }
}

void I2C_WriteData(I2C_TypeDef* I2Cx, uint8_t data){
  // Write the data on the bus
  I2C_SendData(I2Cx, data);
  //Wait until transmission is complete:
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
}

uint8_t I2C_ReadData(I2C_TypeDef* I2Cx){
	uint8_t data;
  // Wait until receive is completed
  while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
  // Read one byte of data from I2c
  data = I2C_ReceiveData(I2Cx);
  return data;
}
