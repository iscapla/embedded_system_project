# Sound visualization with STM3210c-eval board

Components
----------

  - Casabán Planells, Ismael     -> iscapla@live.com
  - Kweun, Min Hyeok             -> minhyeok.Kweun.4523@student.uu.se
  - Stoyanova, Desislava         -> desislava.Stoyanova.4381@student.uu.se

Description of the project
--------------------------

This project has been part of the course in Programming Embedded Systems
that is given each year at Uppsala University (Sweden).

The main idea of the project is to detect noise using four microphones and visualize where the sound signal is coming from. For the visualisation we have used the diplay on the STM3210C-EVAL board, where we represented a 2-Dimensional grid with four quadrants visible on it (one quadrant for each microphone).

As soon as a noise occurs at one of the microphones, a simple dot is supposed to raise up on the respective quadrant. In addition to the STM32 board, we connected also an Arduino Yún board since we met several problems with reading the analog signal from four microphones at once, if they are attached only to the STM32 board. Moreover, in the core of the project, we have an algorithm that determines the absolute or relative locations of points by measurement of distances, using the geometry of circles, spheres or triangles. The process is also called trilateration. However, the microphones happened to be rather cheap equipment and the level of precision and accuracy happened to be a bit low. For this reason, an additional mode of the project has been implemented. Once the user's button  on the board is pressed, the points will be read from the Arduino memory just to demonstrate that the system is working correctly.

Image
-----

![Alt text](/Images/overview.jpg "Overview of the final system.")
![Alt text](/Images/grid.jpg "Grid ploting with a point.")

Hardware
--------
  - STM3210c-eval with STM32f10x
  - Arduino Yún

Software
--------
  - uVision Keil v5.17
  - stm32f10x libraries v3.4.0
  - stm3210c_eval libraries v4.3.0
  - Arduino IDE v1.6.5-r5
  
Methods used/implemented
------------
  - ADC (Arduino and STM32)
  - I2C (communication Arduino-STM32)
  - LCD screen
  - FreeRTOS

Pins used
---------
  - 3.2" TFT LCD with touch screen CN14
  - I2C: PB6(SCL) and PB7(SDA)
  - Button_KEY: PB9
  - 3V3 (different pins)
  - GND (Important to use the same GND as the Arduino)