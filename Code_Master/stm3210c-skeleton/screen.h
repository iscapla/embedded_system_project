#ifndef SCREEN_H
#define SCREEN_H
#include "global.h"
#include "stm3210c_eval_lcd.h"

void errorMessage(char *p);
void showNumLine(int num, uint8_t line);
void setupScreen(void);

#endif
