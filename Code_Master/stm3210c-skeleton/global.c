#include "global.h"
#include "math.h"

xSemaphoreHandle xSemaphore;
xSemaphoreHandle xSemaphoreCentroid;
xSemaphoreHandle xSemaphoreStatus;
Point3D micPos[MICROPHONES];
float micReads[MICROPHONES];
Point3D g_Centroid;
int mask;
myMode status;

void initializeSemaphore(){
	vSemaphoreCreateBinary(xSemaphore);
	vSemaphoreCreateBinary(xSemaphoreCentroid);
	vSemaphoreCreateBinary(xSemaphoreStatus);
	if(!xSemaphore || !xSemaphoreCentroid|| !xSemaphoreStatus){
		errorMessage("Semaphore not       initializated");
	}
}

// To use the system with different position of the microphones, this data must be changed.
// Note that the mask variable is to control that the imput data has no errors. Its value is always
// the MAX distante between two microphones.
void initializeData( void ) {
		mask = sqrt(2*(MAX_HEIGHT*MAX_HEIGHT));
  	micPos[0].x = 0;
    micPos[0].y = 0;
    micPos[0].z = 0;
    
    micPos[1].x = MAX_HEIGHT;
    micPos[1].y = 0;
    micPos[1].z = 0;
	
    micPos[2].x = MAX_HEIGHT;
    micPos[2].y = MAX_HEIGHT;
    micPos[2].z = 0;
	
    micPos[3].x = 0;
    micPos[3].y = MAX_HEIGHT;
    micPos[3].z = 0;
    
		status = MICROPHONE;
}
