#include "microphone.h"
#include "global.h"
#include "i2c.h"
#include "task.h"

const float mult = 8;

void microphonesTask(void *params){
	int receivedData[4];
	int aux = 1;
	vTaskDelay(300/portTICK_RATE_MS);
	while(1){		
		/*----- Receive Phase ------------------------------------------------------*/
		I2C_StartTransmission(I2C1,I2C_Direction_Receiver,SLAVE_DIR);
		
		// Reset the values
		receivedData[0] &= 0;
		receivedData[1] &= 0;
		receivedData[2] &= 0;
		receivedData[3] &= 0;
		
		// Receive 1� integer
		receivedData[0] = I2C_ReadData(I2C1);
		receivedData[0] = receivedData[0] | (I2C_ReadData(I2C1)<<8);

		// Receive 2� integer
		receivedData[1] = I2C_ReadData(I2C1);
		receivedData[1] = receivedData[1] | (I2C_ReadData(I2C1)<<8);
		
		// Receive 3� integer
		receivedData[2] = I2C_ReadData(I2C1);
		receivedData[2] = receivedData[2] | (I2C_ReadData(I2C1)<<8);
		
		// Receive 4� integer
		receivedData[3] = I2C_ReadData(I2C1);
		receivedData[3] = receivedData[3] | (I2C_ReadData(I2C1)<<8);
		
		I2C_GenerateSTOP(I2C1, ENABLE);
		
		while(xSemaphoreTake(xSemaphoreStatus, portMAX_DELAY) != pdTRUE);
		if (status == MICROPHONE){
				aux = mult;
			}else{
				aux = 1;
			}
		xSemaphoreGive(xSemaphoreStatus);
		
		// Update shared data
		while(xSemaphoreTake(xSemaphore, portMAX_DELAY) != pdTRUE);
		micReads[0] = MIN((float)receivedData[0]*aux,mask);
		micReads[1] = MIN((float)receivedData[1]*aux,mask);
		micReads[2] = MIN((float)receivedData[2]*aux,mask);
		micReads[3] = MIN((float)receivedData[3]*aux,mask);
		xSemaphoreGive(xSemaphore);
		
		vTaskDelay(50/portTICK_RATE_MS);
	}     

}

void setupMicrophones(void){
	portBASE_TYPE res;
  res = xTaskCreate(microphonesTask, "microphones",100, NULL, 3, NULL);					
  assert(res == pdTRUE);
}
