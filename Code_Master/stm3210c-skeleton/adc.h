//Header NOT used in this project. But it works :)

#ifndef __ADC_H__
#define __ADC_H__

#include "stm32f10x.h"
#include "stm32f10x_adc.h"

void setupADCReader( void );
uint16_t getDigitalValue(ADC_TypeDef *ADCx, uint8_t Channel);

 #endif
