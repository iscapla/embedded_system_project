#include "stm32f10x.h"
#include "stm3210c_eval_lcd.h"
#include "stm3210c_eval_ioe.h"
#include "algorithm.h"
#include "global.h"
#include <math.h>
#include "task.h"

// struct point c = a + b
#define  ADD(a,b,c) \
	c.x = a.x + b.x; \
	c.y = a.y + b.y; \
	c.z = a.z + b.z; \

// struct point c_p = a_p - b
#define  SUBP(a,b,c) \
        c->x = a->x - b.x; \
        c->y = a->y - b.y; \
        c->z = a->z - b.z; \

#define A micro[0]
#define B micro[1]
#define C micro[2]
#define D micro[3]

#define Ar radius[0]
#define Br radius[1]
#define Cr radius[2]
#define Dr radius[3]


void doTrilateration(Point3D a, Point3D b, Point3D c, float r1, float r2, float r3, Point3D *sol);
void averagePoint(Point3D *p, int num, Point3D *sol);

void AlgorithmTask (void * params){
	
	Point3D micro[MICROPHONES];
	float radius[MICROPHONES];
	Point3D solutions[MICROPHONES];
	Point3D centroid;
    
	// In the beguining of the task, we have to extract the positions of the microphones.
	// If this position change, we have to restart the system.
	// To block the entrance, we can use the same semaphore of the microphone reads.
	while(xSemaphoreTake(xSemaphore, portMAX_DELAY) != pdTRUE);
	A = micPos[0];
	B = micPos[1];
	C = micPos[2];
	D = micPos[3];
	// Release the semaphore
	xSemaphoreGive(xSemaphore);
	
	for(;;){
		
		// Read the values from the shared array (blocking the semaphore)
		while(xSemaphoreTake(xSemaphore, portMAX_DELAY) != pdTRUE);
		Ar = micReads[0];
		Br = micReads[1];
		Cr = micReads[2];
		Dr = micReads[3];
		xSemaphoreGive(xSemaphore);
	
		//With points A B C
		doTrilateration(A,B,C,Ar,Br,Cr,&solutions[0]);
		//With points A B D
		doTrilateration(A,B,D,Ar,Br,Dr,&solutions[1]);
		//With points D C A
		doTrilateration(D,C,A,Dr,Cr,Ar,&solutions[2]);
		//With points D C B
		doTrilateration(D,C,B,Dr,Cr,Br,&solutions[3]);

		averagePoint(solutions, MICROPHONES, &centroid);
		
		while(xSemaphoreTake(xSemaphoreCentroid, portMAX_DELAY) != pdTRUE);
		g_Centroid = centroid;
		xSemaphoreGive(xSemaphoreCentroid);
		
		vTaskDelay(50/portTICK_RATE_MS);
		
	}
	
}

void setupAlgorithm(void){
	portBASE_TYPE res;

  res = xTaskCreate(AlgorithmTask, "algorithm",100, NULL, 2, NULL);
									
  assert(res == pdTRUE);
}


void doTrilateration(Point3D a, Point3D b, Point3D c, float r1, float r2, float r3, Point3D *sol){
	Point3D offset;
	Point3D bb,cc; 		// Used to describe the points with the first one in the center.
	Point3D bbb,ccc;	// Used to store the values after the rotation. aaa is still in the center so we don't have to change it
	float theta;			// Angle to move the cartesian system (in rad.)
	Point3D aux;		// Result of the trilateration
	float d,i,j;			// Auxiliar variables to calculate the trilateration
	float r1r1;			//this aux variable is to precalculate the multiplication, avoiding to do it more than one time


	// Calculate the offset to move the first parameter point to the center. Then, the rest of the points are also moved.
	offset.x = 0 - a.x;
	offset.y = 0 - a.y;
	offset.z = 0 - a.z;

	// Moving the points with the offset
	//ADD(a,offset,aa) //Not necessary
	ADD(b,offset,bb)
	ADD(c,offset,cc)

	// bb must also lie on the x-axis. If not, we must perform a rotation on the view.
	if(bb.y == 0){
		d = bb.x;
		i = cc.x;
		j = cc.y;

		//We need to use r1 and r2 to eliminate y and z from the equation and solve for x:
		//You can search more information about it in: https://en.wikipedia.org/wiki/Trilateration
		//Notice that there are severelar limitations in this algorithm that we do not explain here.
		r1r1 = r1 * r1;
		sol->x = ((r1r1)-(r2*r2)+(d*d))/(2*d);
		sol->y = (((r1r1)-(r3*r3)+(i*i)+(j*j))/(2*j))-(i/j)*(sol->x);
		sol->z = sqrtf(fabsf((r1r1)-(sol->x*sol->x)-(sol->y*sol->y)));

		SUBP(sol,offset,sol)

	}else{
		// To determine the angle of rotation, we can use the dot product equality: A dot B = ||A|| ||B|| cos(theta)
		// For more information: https://en.wikipedia.org/wiki/Dot_product
		// But, we use Trigonometric functions to solve the ecuation. From the ecuation above we can calculate this one.

		// Using the squares  of the point that we want to move into the Y axis. In our case point bb
		theta = atan(fabsf(bb.y)/fabsf(bb.x));

		// Now let's use the rotation matrix to rotate the scene by -theta degrees.
		// More into in: https://en.wikipedia.org/wiki/Rotation_matrix

		// Since bb.y is positive, and the rotation matrix rotates counter-clockwise,
		// we'll use a negative rotation to align bb to the x-axis (if bb.y is negative, don't negate theta).
		if(bb.y > 0){
			theta = theta * (-1);
		}

		bbb.x = (bb.x*cos(theta)) + (bb.y * (-1) * sin(theta));
		bbb.y = (bb.x*sin(theta)) + (bb.y*cos(theta));
		bbb.z = bb.z;

		ccc.x = (cc.x*cos(theta)) + (cc.y * (-1) * sin(theta));
		ccc.y = (cc.x*sin(theta)) + (cc.y*cos(theta));
		ccc.z = cc.z;

		// Same operations used if the Y dimension of the 2nd point is on the X axis
		d = bbb.x;
		i = ccc.x;
		j = ccc.y;

		r1r1 = r1 * r1;
		aux.x = ((r1r1)-(r2*r2)+(d*d))/(2*d);
		aux.y = (((r1r1)-(r3*r3)+(i*i)+(j*j))/(2*j))-(i/j)*(aux.x);
		aux.z = sqrtf(fabsf((r1r1)-(aux.x*aux.x)-(aux.y*aux.y)));

		// Reversing the solution.
		theta = theta * (-1);

		sol->x = (aux.x*cos(theta)) + (aux.y * (-1) * sin(theta));
		sol->y = (aux.x*sin(theta)) + (aux.y*cos(theta));
		sol->z = aux.z;

		// Reversing the translation
		SUBP(sol,offset,sol)
	}
}

void averagePoint(Point3D *p, int num, Point3D *sol){
	int i;
	sol->x = 0;
	sol->y = 0;
	sol->z = 0;

	for(i=0;i<num;i++){
		sol->x += p[i].x;
		sol->y += p[i].y;
		sol->z += p[i].z;
	}

	sol->x = sol->x/num;
	sol->y = sol->y/num;
	sol->z = sol->z/num;
}
