#include <Wire.h>

void setup() {
  Wire.begin(0x20);                // join i2c bus with address 0x10
  Wire.setClock(50000L);           // Set the clock speed to 100KHz
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);           // start serial for output
}

void loop() {
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  Serial.println(howMany);
  while (1 < Wire.available()) { // loop through all but the last
    char c = Wire.read(); // receive byte as a character
    Serial.print("L");
    Serial.println(c);         // print the character
  }
  int x = Wire.read();    // receive byte as an integer
  //Serial.print("Out = ");
  Serial.println(x);         // print the integer
}
