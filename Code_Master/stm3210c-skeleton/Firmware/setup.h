#ifndef SETUP_H
#define SETUP_H

#include <stm32f10x.h>

/*
 * Configure the clocks, GPIO and other peripherals as required by the demo.
 */
void prvSetupHardware(void);
void NVIC_Configuration(void);
void RCC_Configuration(void);
void GPIO_Configuration(void);
void I2C_Configuration(void);


#endif
